//
//  ViewController.swift
//  WeatherApp
//
//  Created by Михаил Копейкин on 30.06.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
    }


}

extension ViewController:UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        
        let urlString = "https://api.weatherstack.com/current?access_key=1e98230a428ae54851c1bf8abdd5c96e&query=\(searchBar.text!)"
        
        let url = URL(string: urlString)
        
        
        var locationName:String?
        var temperature: Double?
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : AnyObject]
                
                if let location = json["location"] {
                    locationName = location["location"] as? String
                }
                
                if let current = json["current"] {
                    temperature = current ["temperature"] as? Double
                }
                
            }
            catch let jsonError {
                print(jsonError)
            }
        }
        task.resume()
    }
}
